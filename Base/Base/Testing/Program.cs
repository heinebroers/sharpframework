using System;
using Base;

namespace Testing
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Container c = Container.getInstance ();
			TestContainerItem ct1 = new TestContainerItem ();
			TestContainerItem ct2 = new TestContainerItem ();
			Guid g1;
			g1 = c.AddItem (ct1);
			Console.WriteLine ("output {0}", g1.ToString());
			Guid g2;
			g2 = c.AddItem (ct2);
			Console.WriteLine ("output {0}", g2.ToString());
			ContainerMessage cm = new ContainerMessage ();
			cm.from = g1;
			c.BroadcastMessage (cm);
			Console.WriteLine ("end");
		}
	}
}
