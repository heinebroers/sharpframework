﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace Base
{
	public class CheckLoop : IContainerItem
	{
		private ContainerItem containerItem;
		Thread oThread;
		private RunningState state;
		Dictionary<Guid,ICheckLoopItem> items;


		public CheckLoop ()
		{
			state = RunningState.Starting;
			containerItem = new ContainerItem (this);
			items = new Dictionary<Guid, ICheckLoopItem>();
			oThread = new Thread(new ThreadStart(CheckLoopThread));
			try 
			{
				oThread.Start();
			}
			catch (ThreadStateException) {
				state = RunningState.Error;
			}
		}
		public void HandleContainerMessage(object sender,ContainerMessage cm) {
			if (cm.to == containerItem.GetGuid())
			{
				switch (cm.cmd)
				{
					case ContainerMessageCommand.Stop:
						state = RunningState.Stopping;
						break;
					case ContainerMessageCommand.Start:
						oThread.Start();
						break;
					case ContainerMessageCommand.Abort:
						oThread.Abort();
						break;
				}
			}
		}
		public void HandleContainerLoop(ContainerMessage cm) {
			if (cm.to == containerItem.GetGuid())
			{
				if (cm.cmd == ContainerMessageCommand.Stop)
				{
					state = RunningState.Stopping;
				}
			}
		}
		public bool HandleSearchLoop(ContainerMessage cm) {
			if (cm.cmd == ContainerMessageCommand.Get) {
				if (cm.cmdParameter.Equals("CheckLoop")) return true;
			}
			return false;
		}
		public void CheckLoopThread() {
			state = RunningState.Running;
			while (state == RunningState.Running)
			{
				Thread.Sleep(500);
			}
			state = RunningState.Stopped;
		}
		public void AddItem(Thread t) {
			Guid g = Guid.NewGuid();
			CheckLoopItem c = new CheckLoopItem(g, t);
			items.Add(g, c);
		}
	}
}

