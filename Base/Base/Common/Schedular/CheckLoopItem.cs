﻿using System;
using System.Threading;


namespace Base
{
	public interface ICheckLoopItem
	{
		DateTime GetNextTime();
		void SetNextTime();
		void SetInterval(int i);
		void Signal();
		void Check();
		void Wait();
	}
	public class CheckLoopItem : ICheckLoopItem {
		AutoResetEvent waitHandle = new AutoResetEvent(false);
		DateTime nextTime;
		int interval;
		Thread thread;
		Guid guid;
		bool signaled = false;

		public CheckLoopItem(Guid g,Thread t) {
			nextTime = DateTime.Now;
			thread = t;
			guid = g;
		}

		public CheckLoopItem(Guid g,Action action) {
			nextTime = DateTime.Now;
			guid = g;

		}

		public virtual DateTime GetNextTime() {
			return nextTime;
		}

		public virtual void SetNextTime() {
			nextTime = nextTime.AddSeconds(interval);
		}

		public virtual void SetInterval(int i) {
			interval = i;
		}
		public virtual void Signal() {
			signaled = true;
			waitHandle.WaitOne();
		}
		public Guid GetGuid() {
			return guid;
		}
		public Thread GetThread() {
			return thread;
		}
		public virtual void Check() {
			if (DateTime.Now >= nextTime)
			{
				Signal();
				signaled = false;
				SetNextTime();
			}
		}
		/// <summary>
		/// Wait this instance.
		/// A thread must call this to wait
		/// </summary>
		public virtual void Wait() {
			waitHandle.Set();
			if (interval > 0)
				Thread.Sleep(2000 * interval);
			else
			{
				while (!signaled)
				{
					Thread.Sleep(1000);
				}
				signaled = false;
			}
		}
	}
}

