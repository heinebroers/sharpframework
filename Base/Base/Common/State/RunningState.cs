﻿using System;

namespace Base
{
	public enum RunningState
	{
		Initializing, Starting, Stopping, Running, Paused, Stopped, Error
	}
}

