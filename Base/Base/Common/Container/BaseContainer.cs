﻿using System;
using System.Collections.Generic;

namespace Base
{
	public interface IBaseContainer {
		Guid AddItem(ContainerItem c);
		void RemoveItem (Guid g);
		IContainerItem GetItem (Guid g);
		void BroadcastMessage (ContainerMessage cm);
		void LoopItems (ContainerMessage cm);
		IContainerItem SearchItem (ContainerMessage cm);
		List<IContainerItem> SearchMultipleItems (ContainerMessage cm);
		void SetKey (string sName, object cv);
		object GetKey (string sName);
	}
	public class BaseContainer
	{
		private static Dictionary<Guid,IContainerItem> containerItems = new Dictionary<Guid,IContainerItem>();
		private static Dictionary<string,object> dict = new Dictionary<string,object>();
		private Guid guid = Guid.NewGuid ();
		EventHandler<ContainerMessage> broadcaster;

		public BaseContainer ()
		{
		}

		public Guid AddItem(IContainerItem c) {
			Guid g;
			g = Guid.NewGuid ();
			containerItems.Add (g,c);
			broadcaster += c.HandleContainerMessage;
			return g;
		}
		public void RemoveItem(Guid g) {
			containerItems.Remove (g);
		}
		public IContainerItem GetItem(Guid g) {
			IContainerItem val;
			if (!containerItems.TryGetValue (g, out val)) {
				return null;
			}
			return val;
		}
		public Guid GetGuid() {
			return guid;
		}

		public void BroadcastMessage(ContainerMessage cm) {
			broadcaster.Invoke (this, cm);
		}
		public void LoopItems(ContainerMessage cm) {
			foreach (KeyValuePair<Guid,IContainerItem> p in containerItems) {
				IContainerItem c = p.Value;
				c.HandleContainerLoop (cm);
			}
		}
		public IContainerItem SearchItem(ContainerMessage cm) {
			foreach (KeyValuePair<Guid,IContainerItem> p in containerItems) {
				IContainerItem c = p.Value;
				if (c.HandleSearchLoop (cm))
					return p.Value;
			}
			return null;
		}
		public List<IContainerItem> SearchMultipleItems(ContainerMessage cm) {
			List<IContainerItem> ret = null;
			foreach (KeyValuePair<Guid,IContainerItem> p in containerItems) {
				IContainerItem c = p.Value;
				if (c.HandleSearchLoop (cm)) {
					if (ret == null)
						ret = new List<IContainerItem> ();
				}
				ret.Add(p.Value);
			}
			return ret;
		}
		public List<Guid> SearchMultipleGuid(ContainerMessage cm) {
			List<Guid> ret = null;
			foreach (KeyValuePair<Guid,IContainerItem> p in containerItems) {
				IContainerItem c = p.Value;
				if (c.HandleSearchLoop (cm)) {
					if (ret == null)
						ret = new List<Guid> ();
				}
				ret.Add(p.Key);
			}
			return ret;
		}
		public void SetKey(string sName,object cv ) {
			if (dict.ContainsKey(sName)) {
				dict.Remove (sName);
			}
			dict.Add (sName, cv);
		}
		public object GetKey(string sName) {
			object val;
			if (dict.TryGetValue(sName,out val)) return val;
			return null;
		}
	}
}

