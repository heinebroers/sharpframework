using System;

namespace Base
{
	public class Container : BaseContainer
	{
		private static Container container = new Container();
		/// <summary>
		/// Initializes a new instance of the <see cref="Base.Container"/> class.
		/// </summary>
		private Container () {
		}

		public static Container getInstance() {
			return container;
		}
	}
}

