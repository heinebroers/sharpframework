using System;

namespace Base
{
	public interface IContainerItem {
		void HandleContainerMessage(object sender,ContainerMessage cm);
		void HandleContainerLoop(ContainerMessage cm);
		bool HandleSearchLoop(ContainerMessage cm);
	}
	/// <summary>
	/// Container item: Class for adding functionality to the container
	/// </summary>
	public class ContainerItem: EventArgs, IContainerItem {
		private Guid guid;
	    public ContainerItem() {
	    }
		public ContainerItem(IContainerItem c) {
			guid = Container.getInstance().AddItem(c);
		}
		public virtual void HandleContainerMessage(object sender,ContainerMessage cm) {
		}
		public virtual void HandleContainerLoop(ContainerMessage cm) {
		}
		public virtual bool HandleSearchLoop(ContainerMessage cm) {
			return false;
		}
		public Guid GetGuid() {
			return guid;
		}
	}
	/// <summary>
	/// For copying purposes
	/// </summary>
	public class DummyContainerItem : IContainerItem {
		
		public void HandleContainerMessage(object sender,ContainerMessage cm) {
		}
		public void HandleContainerLoop(ContainerMessage cm) {
		}
		public bool HandleSearchLoop(ContainerMessage cm) {
			return false;
		}
	}
}

