﻿using System;
using System.Collections.Generic;

namespace Base
{
	public class RecursiveContainer : BaseContainer, IContainerItem
	{
		Dictionary<Guid,BaseContainer> containers = new Dictionary<Guid, BaseContainer>();

		public RecursiveContainer ()
		{
		}
		public void HandleContainerMessage(object sender,ContainerMessage cm) {
		}
		public void HandleContainerLoop(ContainerMessage cm) {
		}
		public bool HandleSearchLoop(ContainerMessage cm) {
			return false;
		}
		public Guid AddContainer() {
			BaseContainer b = new BaseContainer ();
			containers.Add (b.GetGuid (), b);
			return b.GetGuid ();
		}
		public void RemoveContainer(Guid g) {
			containers.Remove (g);
		}
	}
}

