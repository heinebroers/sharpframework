using System;

namespace Base
{
	public enum ContainerMessageCommand { Get, Start, Stop, Pause, Abort };

	public class ContainerMessage: EventArgs
	{
		public Guid from;
		public Guid to;
		public ContainerMessageCommand cmd;
		public string cmdParameter;
		public ContainerMessageContent body;
		public ContainerMessage() {
		}
	}
}

